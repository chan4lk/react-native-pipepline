#
# GitLab CI: React Native @ Android
#
# https://gitlab.com/wldhx/gitlab-ci-react-native-android
#

FROM registry.gitlab.com/wldhx/gitlab-ci-android:master
MAINTAINER Dmitriy Volkov <wldhx@wldhx.me>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      gnupg && \

    # Yarn
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \

    # Node 8.x
    curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
    echo 'deb https://deb.nodesource.com/node_8.x xenial main' > /etc/apt/sources.list.d/nodesource.list && \

    apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      nodejs \
      yarn
